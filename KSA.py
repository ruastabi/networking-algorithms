import sys
S=[x for x in range(256)]		#Initial vector, array from 0 to 255
K=input("Give me the key: ")		#Key (Size 16)
T=[0 for x in range(256)]		#Vector repeating the key 16 times

c=0
for x in range(256):
	T[x]=K[c]
	c+=1
	if c>=len(K):
		c=0
j=0
for i in range(256):
	j=(j + S[i] + ord(T[i])) % 256
	temp=S[i]
	S[i]=S[j]
	S[j]=temp

f= open("S-KSA.txt","w+")
for i in range(256):
	f.write(str(S[i]))
	f.write("\n")

f.close()
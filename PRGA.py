import sys
#Recoge el vector S final del KSA y devuelve una clave de cifrado con la longitud del mensaje a cifrar
file=input("Give me the generated vector file: ")
f=open(file,"r")
data=f.readlines()
S=[0 for x in range(256)]
for i in range(256):
	S[i]=int(data[i])

i=0
j=0
k=0

M=input("Give me a message: ")
K=[0 for x in range(len(M))]

while(k<len(M)):
	i=(i+1) % 256
	j=(j+S[i]) % 256
	temp=S[i]
	S[i]=S[j]
	S[j]=temp
	t= (S[i] + S[j] ) % 256
	K[k]=S[t]
	k+=1

f= open("K-PRGA.txt","w+")
for i in range(len(M)):
	f.write(str(K[i]))
	f.write("\n")

f.close()